import React from "react";
import ReactDOM from "react-dom";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import renderer from "react-test-renderer";
import ItemsList from "../components/ItemsList/ItemsList";

afterEach(cleanup);

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ItemsList />, div);
});

it("renders ItemsList correctly", () => {
  render(<ItemsList />);
});

it("matches snapshot", () => {
  const tree = renderer.create(<ItemsList />).toJSON();
  expect(tree).toMatchSnapshot();
});
