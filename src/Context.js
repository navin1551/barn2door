import React from "react";

const Context = React.createContext({
  searchResults: []
});

export default Context;
