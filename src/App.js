import React from "react";
import SearchInput from "./components/SearchInput/SearchInput";
import ItemsList from "./components/ItemsList/ItemsList";
import Context from "./Context";
import "./App.css";

export default class App extends React.Component {
  state = {
    searchResults: []
  };

  searchSubmitHandle = results => {
    this.setState({
      searchResults: results
    });
  };

  render() {
    const contextValue = {
      searchResults: this.state.searchResults,
      searchSubmitHandle: this.searchSubmitHandle
    };
    return (
      <Context.Provider value={contextValue}>
        <div className="app">
          <section>
            <h1 id="main-title">Barn2Door</h1>
          </section>
          <section>
            <SearchInput />
            <ItemsList />
          </section>
        </div>
      </Context.Provider>
    );
  }
}
