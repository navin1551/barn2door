import React from 'react';
import './Item.css';

export default class Item extends React.Component {
	render() {
		let image = this.props.img ?
			(<img src={this.props.img} alt="item pictures" className="item-pictures" />) :
			(<p>Image Unavailable At This Time</p>);

		let description = this.props.desc ?
			<p>{this.props.desc}</p> :
			<p>Description Unavailable At This Time</p>;

		let categories = this.props.categories.map((categories, i) => <p key={i}>{categories.name}</p>);

		return (
			<div className="item-area">
				<li>
					<h2 id="item-title">{this.props.title}</h2>
					{categories}
					{image}
					{description}
				</li>
			</div>
		);
	}
}
