import React from 'react';
import Context from '../../Context';
import ExampleData from '../../example_data';
import './SearchInput.css';

export default class SearchInput extends React.Component {
	state = {
		query: '',
		noResultsMessage: false,
		inValidSearchMessage: false
	};

	static contextType = Context;

	inputSubmitHandle = (e) => {
		e.preventDefault();
		let searchItem = this.state.query.toLowerCase().trim().replace(/[^a-z0-9,. ]/gi, '');

		if (!searchItem) {
			this.context.searchSubmitHandle([]);
			return this.setState({
				noResultsMessage: true,
				inValidSearchMessage: false
			});
		}

		const results = ExampleData.items.filter((item) => {
			// destructuring item to remove img and description field out of query
			const { img, description, ...restItem } = item;
			return JSON.stringify(restItem).toLowerCase().includes(searchItem);
		});

		if (!results.length) {
			this.context.searchSubmitHandle([]);
			return this.setState({
				inValidSearchMessage: true,
				noResultsMessage: false
			});
		}

		this.context.searchSubmitHandle(results);

		this.setState({
			query: '',
			noResultsMessage: false,
			inValidSearchMessage: false
		});
	};

	inputChangeHandle = (e) => {
		this.setState({
			query: e.target.value
		});
	};

	render() {
		let errorMessage = this.state.noResultsMessage ? 'Enter Search Value' : '';
		let invalidMessage = this.state.inValidSearchMessage ? 'No Results Found' : '';
		return (
			<div>
				<form onSubmit={this.inputSubmitHandle}>
					<label>Search:</label>
					<input type="text" onChange={this.inputChangeHandle} value={this.state.query} />
					<button id="search-button">Search Now</button>
				</form>
				<div id="error-message">
					{errorMessage}
					{invalidMessage}
				</div>
			</div>
		);
	}
}
