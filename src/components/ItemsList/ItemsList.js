import React from "react";
import Item from "../Item/Item";
import Context from "../../Context";

export default class ItemsList extends React.Component {
  static contextType = Context;

  render() {
    let listResults = this.context.searchResults.map(item => (
      <Item
        categories={item.categories}
        title={item.title}
        img={item.img}
        desc={item.description}
        key={item.id}
      />
    ));

    return (
      <div>
        <ul>{listResults}</ul>
      </div>
    );
  }
}
