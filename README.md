1)How do we run your app? (ex. npm install and then npm start)

To run this app, run npm install first to install any dependencies and then run npm start
to run the app locally on your computer.

2)How long did it take you to complete this assignment (in hours)?

This assignment took me around 2 hours to complete which included the CSS styling
and React testing.

3)What did you enjoy the most while working on this assignment?

What I enjoyed most about this assignment was working with JSON data that had multiple
nested objects that weren't consistent across the JSON file. I enjoyed setting up
conditional checks to first make sure that nested object was even present and then
manipulating it. I also love working with React and its my favorite framework to use
so anytime I get to build a project or app with it is always fun for me.

4)What did you learn (if anything) while working on this assignment? (optional)

I learned about different version control systems. I have only had experience using GitHub
up to this point so using GitLab was new to me and it was nice getting to use something new.
